package main

import (
	"context"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"sync"
	"time"
)

const (
	timeFormat = "2006-01-02 15:04:05.999999999"

	tradingDayOpeningTime = "2019-01-30 07:00:00.000000"
	tradingDayClosingTime = "2019-01-31 03:00:00.000000"

	candle1 = 5
	candle2 = 30
	candle3 = 240

	timeoutSeconds = 5
)

type Trade struct {
	Ticker    string
	Price     string
	Amount    string
	Timestamp time.Time
}

type Candle struct {
	Ticker     string
	Timestamp  time.Time
	OpenPrice  string
	MaxPrice   string
	MinPrice   string
	ClosePrice string
}

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*timeoutSeconds)
	defer cancel()

	var filename string

	flag.StringVar(&filename, "file", "trades.csv", "file name with trades")
	flag.Parse()

	openDay, err := time.Parse(timeFormat, tradingDayOpeningTime)
	if err != nil {
		log.Fatalf("error occurred while a try to parse time. %s", err)
	}

	closeDay, err := time.Parse(timeFormat, tradingDayClosingTime)
	if err != nil {
		log.Fatalf("error occurred while a try to parse time. %s", err)
	}

	start := make(chan struct{})

	done, err := createPipeline(filename, openDay, closeDay, start)
	if err != nil {
		log.Fatal(err)
	}

	go func() {
		start <- struct{}{}
	}()

	select {
	case <-ctx.Done():
		log.Fatalf("program has stopped work by a timeout: %d seconds", timeoutSeconds)
	case <-done:
	}
}

func createPipeline(filename string, openDay time.Time, closeDay time.Time, start <-chan struct{}) (<-chan struct{}, error) {
	trades, errors, err := readTrades(filename, start)
	if err != nil {
		return nil, err
	}

	t5 := make(chan Trade)
	c5 := calculateCandle(t5, openDay, closeDay, time.Duration(candle1))

	done5, err5, err := writeFile(fmt.Sprintf("candles_%dmin.csv", candle1), c5)
	if err != nil {
		return nil, err
	}

	t30 := make(chan Trade)
	c30 := calculateCandle(t30, openDay, closeDay, time.Duration(candle2))

	done30, err30, err := writeFile(fmt.Sprintf("candles_%dmin.csv", candle2), c30)
	if err != nil {
		return nil, err
	}

	t240 := make(chan Trade)
	c240 := calculateCandle(t240, openDay, closeDay, time.Duration(candle3))

	done240, err240, err := writeFile(fmt.Sprintf("candles_%dmin.csv", candle3), c240)
	if err != nil {
		return nil, err
	}

	go func() {
		for {
			trade, ok := <-trades
			if !ok {
				break
			}
			t5 <- trade
			t30 <- trade
			t240 <- trade
		}
		close(t5)
		close(t30)
		close(t240)
	}()

	handleErrors(errors, err5, err30, err240)

	return waitAll(done5, done30, done240), nil
}

func calculateCandle(trades <-chan Trade, openDay time.Time, closeDay time.Time, candleDuration time.Duration) chan Candle {
	candles := make(chan Candle)

	tempCandles := make(map[string]Candle)
	openCandleTime := openDay
	closeCandleTime := openCandleTime.Add(time.Minute * candleDuration)

	go func() {
		for {
			trade, ok := <-trades
			if !ok {
				break
			}

			if trade.Timestamp.After(closeDay) {
				openDay, closeDay = incrementTradeDay(openDay, closeDay)
				openCandleTime, closeCandleTime = updateCandleTimes(openDay, candleDuration)

				for _, tempCandle := range tempCandles {
					candles <- tempCandle
				}

				tempCandles = make(map[string]Candle)
			}

			if trade.Timestamp.Before(openDay) {
				continue
			}

			if trade.Timestamp.After(closeCandleTime) {
				for trade.Timestamp.After(closeCandleTime) {
					openCandleTime, closeCandleTime = incrementCandlePeriod(openCandleTime, closeCandleTime, candleDuration)
				}

				for _, tempCandle := range tempCandles {
					candles <- tempCandle
				}

				tempCandles = make(map[string]Candle)
			}

			addCandleToIfNotExists(tempCandles, trade, openCandleTime)
			tempCandles[trade.Ticker] = tempCandles[trade.Ticker].updateCandle(trade)
		}

		for _, tempCandle := range tempCandles {
			candles <- tempCandle
		}

		close(candles)
	}()

	return candles
}

func updateCandleTimes(openDay time.Time, candlePeriod time.Duration) (time.Time, time.Time) {
	openCandleTime := openDay
	closeCandleTime := openCandleTime.Add(time.Minute * candlePeriod)

	return openCandleTime, closeCandleTime
}

func incrementTradeDay(openDay time.Time, closeDay time.Time) (time.Time, time.Time) {
	openDay = openDay.AddDate(0, 0, 1)
	closeDay = closeDay.AddDate(0, 0, 1)

	return openDay, closeDay
}

func incrementCandlePeriod(openCandleTime time.Time, closeCandleTime time.Time, candlePeriod time.Duration) (time.Time, time.Time) {
	openCandleTime = closeCandleTime
	closeCandleTime = openCandleTime.Add(time.Minute * candlePeriod)

	return openCandleTime, closeCandleTime
}

func (candle Candle) updateCandle(trade Trade) Candle {
	if candle.MaxPrice < trade.Price {
		candle.MaxPrice = trade.Price
	}

	if candle.MinPrice > trade.Price {
		candle.MinPrice = trade.Price
	}

	candle.ClosePrice = trade.Price

	return candle
}

func addCandleToIfNotExists(m map[string]Candle, trade Trade, timestamp time.Time) {
	_, ok := m[trade.Ticker]
	if !ok {
		m[trade.Ticker] = trade.createCandle(timestamp)
	}
}

func (trade Trade) createCandle(timestamp time.Time) Candle {
	return Candle{
		Ticker:     trade.Ticker,
		Timestamp:  timestamp,
		OpenPrice:  trade.Price,
		MaxPrice:   trade.Price,
		MinPrice:   trade.Price,
		ClosePrice: trade.Price,
	}
}

func readTrades(filename string, start <-chan struct{}) (chan Trade, chan error, error) {
	trades := make(chan Trade)
	errors := make(chan error)

	file, errFile := os.Open(filename)
	if errFile != nil {
		return nil, nil, fmt.Errorf("an error occurred while opening file %s: %s", filename, errFile)
	}

	reader := csv.NewReader(file)

	go func() {
		<-start

		for {
			line, err := reader.Read()
			if err == io.EOF {
				break
			}

			if err != nil {
				errors <- fmt.Errorf("an error occurred while trying to read a line from a file: %s", err)
				continue
			}

			timeParse, err := time.Parse(timeFormat, line[3])
			if err != nil {
				errors <- fmt.Errorf("error occurred while a try to parse time. %s", err)
			}

			trade := Trade{
				Ticker:    line[0],
				Price:     line[1],
				Amount:    line[2],
				Timestamp: timeParse,
			}

			trades <- trade
		}

		close(trades)
		close(errors)

		defer file.Close()
	}()

	return trades, errors, errFile
}

func writeFile(filename string, candles <-chan Candle) (chan struct{}, chan error, error) {
	done := make(chan struct{})
	errors := make(chan error)

	file, err := os.Create(filename)
	if err != nil {
		return nil, nil, fmt.Errorf("error occurred while a try to create a file. %s", err)
	}

	writer := csv.NewWriter(file)

	go func() {
		for {
			candle, ok := <-candles
			if !ok {
				break
			}

			var line []string
			line = append(line, candle.Ticker, candle.Timestamp.Format(time.RFC3339), candle.OpenPrice, candle.MaxPrice, candle.MinPrice, candle.ClosePrice)

			err := writer.Write(line)
			if err != nil {
				errors <- fmt.Errorf("error occurred while a try to write to a file. %s", err)
			}

			writer.Flush()
		}

		done <- struct{}{}

		defer writer.Flush()

		close(done)
		close(errors)

		defer file.Close()
	}()

	return done, errors, nil
}

func handleErrors(channels ...<-chan error) {
	f := func(errors <-chan error) {
		for {
			err, ok := <-errors
			if !ok {
				break
			}

			log.Println(err)
		}
	}

	for _, channel := range channels {
		go f(channel)
	}
}

func waitAll(channels ...<-chan struct{}) <-chan struct{} {
	done := make(chan struct{})

	var wg sync.WaitGroup

	f := func(channel <-chan struct{}) {
		<-channel
		wg.Done()
	}

	wg.Add(len(channels))

	for _, channel := range channels {
		go f(channel)
	}

	go func() {
		wg.Wait()
		done <- struct{}{}
	}()

	return done
}
